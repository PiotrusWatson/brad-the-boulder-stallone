extends RigidBody3D

var damage
signal hurt_player(target, damage)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_area_3d_body_entered(body):
	if body.is_in_group("player"):
		hurt_player.emit(damage, body)
	print(body)
	if body != self and !body.is_in_group("enemies"):
		queue_free()
		
