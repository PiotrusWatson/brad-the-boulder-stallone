extends KinematicBody
"""
export(bool) var flyMode = false setget setFlyMode

export(float, 0, 3) var lookTurnIntensity = 1
onready var _lookTurnIntensity = lookTurnIntensity

export(int) var maximumHealth = 200
export(int) var defaultHealth = 100
onready var health = defaultHealth
const LOW_HEALTH = 30
onready var lowHealth = (health <= LOW_HEALTH)

export(int) var maximumArmour = 200
export(int) var defaultArmour = 100
onready var armour = 0

export(int) var maximumSprint = 200
export(int) var defaultSprint = 100
onready var sprint = defaultSprint setget setSprent

export(bool) var multiPlayer = false
var currentPlayerAnim = "MachinieIdle"

const MAX_EE_PROTECTOR_HALTH = 100
var eeProtectorHalth = 0 setget setEeProtectorHealth
const MAX_EE_HALTH = 150
onready var eeHalth = float(MAX_EE_HALTH) setget setEeHalth
var kill_timer = 0

var interact = false

enum {
	INACTIVE,
	LEVELEND,
	DEED,
	STAUNDIN,
	MUIVIN,
	CROOCHIN,
	CROOCHMUIVE,
	JIMP,
	SPRENT,
	CATCHBREATH,
	AIM,
	FLEE,
	CHYNGE,
	JIMPSPRENT,
	CHEAT
}
var currentState = STAUNDIN setget setState, getState # The current state o the player.
onready var previousState = currentState
var defaultState = 0

var camera_angle = 0
const MOUSE_MODIFIER = 75
onready var _mouse_sensitivity = float(config.get_value("Config", "MouseSensitivity")) / MOUSE_MODIFIER setget setMouse
onready var mouse_sensitivity = _mouse_sensitivity
var camera_change = Vector2()
onready var onAndroid = OS.get_name().match("Android")

var velocity = Vector3()
var previousVelocity = 0
var direction = Vector3()
var direzione = 0
var controller = false

# fly variables
const FLY_SPEED = 20
const FLY_ACCEL = 4

#walk variables
var gravity = -9.8 * 3
var MAX_SPEED = 16
const MAX_RUNNING_SPEED = 30
const MAX_REKIVERIN_SPEED = 10
const MAX_ADS_SPEED = 1
const EXPLODE_SPEED = 20
var ACCEL = 10
var DEACCEL = 7
var bounce = false

#jumping
var jump_height = 10
var has_contact = false

#slipe variables
const MAX_SLOPE_ANGLE = 100

#stair variables
const MAX_STAIR_SLOPE = 20
const STAIR_JUMP_HEIGHT = 6

onready var currentAnim setget setAnim

var lastShot
var look_direction = Vector3()

var haesBody = true
var haesSFX = false
var isInLevel = false
onready var shuitRay = $"Head/Camera/FPS Arms/shuitRay"

onready var cameraY = $Head/Camera.translation.y
const EXPLOSION_COOLER = 100
var explosion_timer = 0 setget set_explosion_timer
onready var timer_slowdown = explosion_timer
var explosion_severity = 0.1

const LAND_COOLER = 3
var land_timer = 0

#export(String) var staundinAnimation = "FPSStaundin"
#export(PoolStringArray) var walkinAnimations # 0 = up, 1 = doun, 2 = left an 3 = richt
var currentDirection = 0 # This will determine what direction the model will be going for the animation.
#export(String) var croochAnimation = "FPSCrooch"
#export(String) var jimpAnimation = "FPSJimpin"
#export(String) var sprentAnimation = "FPSSprint"

var yaw = 0
var pitch = 0
onready var prevPitch = pitch
onready var prevYaw = yaw
const TURN_COOLER = 0.01
onready var turnTimer = TURN_COOLER
const LOOK_COOLER = 0.005
onready var lookTimer = LOOK_COOLER

var rekiveryTime = 10
var previousSpeed

export(Array) var inventory

var hitSource

signal setSprint
signal unlockWapen
signal unlockGadget
signal chyngeSprent
signal deid
signal turnArms
signal setActive
signal returnResult
signal shootProjectile
signal noclip

signal somethingDetected
signal gatKeycaird

signal playLocalSFX
signal applySFXRange

signal shawHalth
signal lowHalth
signal addArmour
signal addAmmo
signal showHideHUDMouse
signal getObjectives
signal addWapenAmmo
signal disguise

signal playSFX

signal toggleGoggles

signal showObject

signal updateState

func _ready():
	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")
	
	# Called every time the node is added to the scene.
	# Initialization here
	if !has_node("Head/FPSBody"):
		haesBody = false
	if has_node("SoondFX"):
		print(connect("playSFX", $"SoondFX", "play"))
	
	if has_node("SoondFX"):
		print(connect("playLocalSFX", $SoondFX, "play"))
	
	previousSpeed = MAX_SPEED

func _process(delta):		
		if turnTimer > 0:
			turnTimer -= delta
		if lookTimer > 0:
			lookTimer -= delta
		
		#Gin the player is sprentin (state 4), deplete the sprent amoont 'till hit gits tae 0.
		#Gin it reaches there, chynge the state tae catchin breath (5), whaur ye canna rin, jump or crooch.
		#Gin the player lats gae o the sprent command afore that, increase the sprent back, an function like ordinar.
		if currentState == SPRENT:
			if velocity.length() > ACCEL:
				self.sprint -= delta * 50
			if sprint <= 0:
				self.currentState = CATCHBREATH
				emit_signal("setSprint", false)
		elif direction.length() > 0 and currentState != CROOCHIN and currentState != AIM:
			if sprint < defaultSprint:
				if !Input.is_action_pressed("move_sprint"):
					self.sprint += delta * rekiveryTime
				previousSpeed = MAX_REKIVERIN_SPEED
			else:
				previousSpeed = MAX_SPEED
		elif currentState != JIMP:
			if sprint < defaultSprint:
				self.sprint += delta * 30
	
	aim() # This caas the function that taks care o the swayin an muivement on the FPS airms while muivin
	
	if Demo.demoMode:
		$Head/Camera/Area/CollisionShape.disabled = true

func setBack(hitSource, delta):
	velocity -= move_and_slide((hitSource - global_transform.origin).normalized() * delta * velocity)

func _physics_process(delta):
	if currentState > DEED:
		if currentState == FLEE or flyMode:
			fly(delta)
		else:
			walk(delta)
		if multiPlayer:
			emit_signal("updateState", global_transform.origin, $Head.rotation.y, currentPlayerAnim, health)


func _input(event):	
	if currentState > DEED and canAim():
		if event is InputEventMouseMotion and !onAndroid: # Gin the moose is muivin
			if graphics.inverseY:
				event.relative.y = -event.relative.y
			if lookTimer <= 0:
				camera_change = event.relative * mouse_sensitivity
				yaw = fmod(yaw - event.relative.x, 360)
				pitch = max(min(pitch - event.relative.y, 90), -90)
				lookTimer = LOOK_COOLER
				_lookTurnIntensity = lookTurnIntensity
		elif event is InputEventKey or event is InputEventAction:
			if event.is_action_pressed("use"):
				use()
				interact = true
			else:
				interact = false

# func use():
	# if $Head/Camera/meleeRay.is_colliding():
		# var collider = $Head/Camera/meleeRay.get_collider()
		# if collider.is_in_group("Door"):
			# collider.tryDoor(self)
		# elif collider.is_in_group("Interactive"):
			# if collider.interactObject.match($"Head/Camera/FPS Arms".gadgetName) or collider.interactObject.empty():
				# collider.interact()
				# interact = true
			# elif collider.has_method("wrongInteract"):
				# collider.wrongInteract()
			# if  collider.is_in_group("PC"):
				# emit_signal("playSFX", "Keyboard")

func walk(delta):
	# reset the direction o the player
	direction = Vector3()
	direzione = -1
	
	# git the rotation o the camera
	var aim = $Head/Camera.get_global_transform().basis
	
	# chack inpit an chynge direction
	if Input.is_action_pressed("move_forward"):
		direzione = 0
		direction -= aim.z
		if multiPlayer:
			if !"shuit" in currentPlayerAnim and !"Jimp" in currentPlayerAnim:
				currentPlayerAnim = "MachinieRin"
	elif Input.is_action_pressed("move_backward"):
		if currentState != SPRENT:
			direzione = 1
			direction += aim.z
			if multiPlayer:
				if !"shuit" in currentPlayerAnim and !"Jimp" in currentPlayerAnim:
					currentPlayerAnim = "wawkinbackwart"
	
	if Input.is_action_pressed("move_left"):
		if currentState != SPRENT:
			direzione = 2
			direction -= aim.x
			if multiPlayer:
				if !"shuit" in currentPlayerAnim and !"Jimp" in currentPlayerAnim:
					currentPlayerAnim = "wawkinside"
	elif Input.is_action_pressed("move_right"):
		if currentState != SPRENT:
			direzione = 3
			direction += aim.x
			if multiPlayer:
				if !"shuit" in currentPlayerAnim and !"Jimp" in currentPlayerAnim:
					currentPlayerAnim = "wawkinside"
	
	if Input.get_connected_joypads().size() >= 1:
		joy_input(aim)
	elif OS.has_touchscreen_ui_hint():
		aim_input()
	
	direction.y = 0
	
	direction = direction.normalized()
	
	if is_on_floor():
		if currentState == JIMP:
			land_timer = LAND_COOLER
			if graphics.vibration:
				Input.start_joy_vibration(0, 0.5, 0.5, 0.2)
			self.currentState = defaultState
		elif !Demo.demoMode and currentState != SPRENT and currentState != AIM:
			if direction.length() > 0:
				if Input.is_action_pressed("crooch") or $Heid.is_colliding():
					land_timer = 0
					self.currentState = CROOCHMUIVE
				elif !$Heid.is_colliding():
					self.currentState = MUIVIN
			else:
				if Input.is_action_pressed("crooch") or $Heid.is_colliding():
					land_timer = 0
					self.currentState = CROOCHIN
				elif !$Heid.is_colliding():
					self.currentState = STAUNDIN
		
		if Input.is_action_pressed("move_sprint") and (Input.is_action_pressed("move_forward") or Input.get_action_strength("move_forward_analog") > 0.5):
			if sprint >= defaultSprint / 5:
				self.currentState = SPRENT
				emit_signal("setSprint", true)
		elif sprint < defaultSprint and currentState != AIM:
			self.currentState = defaultState
			emit_signal("setSprint", false)
		else:
			emit_signal("setSprint", false)
#		else:
#			if direction.length() > 0:
#				if haesBody:
#					self.currentAnim = walkinAnimations[direzione]
#				rekiveryTime = 10
#			else:
#				self.currentAnim = staundinAnimation
#				rekiveryTime = 30
		has_contact = true
		
		if is_on_wall():
			var n = $Tail.get_collision_normal()
			var floor_angle = rad2deg(acos(n.dot(Vector3(0, 1, 0))))
			if floor_angle > MAX_SLOPE_ANGLE:
				velocity.y += gravity * delta
	else:
		if velocity.y > 0:
			if $Heid.is_colliding():
				velocity.y = 0
				if $Tail.is_colliding():
					currentState = CROOCHIN
				else:
					move_and_collide(Vector3(0, -0.1, 0))
		if !$Tail.is_colliding():
			self.currentState = JIMP
			has_contact = false
			previousVelocity = velocity.y
		velocity.y += gravity * delta
	
#	if has_contact and !is_on_floor():
#		move_and_collide(Vector3(0, -0.1, 0))
	
	
	var temp_velocity = velocity
	temp_velocity.y = 0
	
	var speed = previousSpeed
	# where would the player gang at max speed
	var target = direction * speed
	
	var acceleration
	if direction.dot(temp_velocity) > 0:
		acceleration = ACCEL
	else:
		acceleration = DEACCEL
	
	# calculate the poriton o the distance tae gang
	temp_velocity = temp_velocity.linear_interpolate(target, acceleration * delta)
	
	velocity.x = temp_velocity.x
	velocity.z = temp_velocity.z
	
	if currentState != JIMP and currentState != AIM and !$Heid.is_colliding():
		if has_contact and Input.is_action_just_pressed("jump"):
			velocity.y = jump_height
			has_contact = false
			self.currentState = JIMP
		elif bounce:
			velocity.y = jump_height * 2
			if is_on_wall():
				velocity.y = jump_height * 3
	
	# muive
	velocity = move_and_slide(velocity, Vector3.UP)
	#$Head/Camera/ViewportContainer/Viewport/GunCam.global_transform = $Head/Camera.global_transform

func fly(delta):
	# reset the direction o the player
	direction = Vector3()
	
	# git the rotation o the camers
	var aim = $Head/Camera.get_global_transform().basis
	
	# chack inpit an chynge direction
	if Input.is_action_pressed("move_forward"):
		direction -= aim.z
	elif Input.is_action_pressed("move_backward"):
		direction += aim.z
	if Input.is_action_pressed("move_left"):
		direction -= aim.x
	elif Input.is_action_pressed("move_right"):
		direction += aim.x
	
	if Input.get_connected_joypads().size() >= 1:
		joy_input(aim)
	
	direction = direction.normalized()
	
	# where would the player gang at max speed
	var target = direction * FLY_SPEED
	
	# calculate the poriton o the distance tae gang
	velocity = velocity.linear_interpolate(target, FLY_ACCEL * delta)
	
	#muive
	move_and_slide(velocity)

func aim():
	#var tempPitch = pitch - prevPitch
	#var tempYaw = yaw - prevYaw
	
	if camera_change.length() > 0 and currentState != AIM:
		#yaw = -camera_change.x * mouse_sensitivity
		#pitch = -camera_change.y * mouse_sensitivity
		
		#Muive sideweys based on x moose muivement.
		$Head.rotate_y(deg2rad(-camera_change.x))
		
		#Muive up an doon based on 4 moose muivement, but no gane ony mair up 
		#or doon bi 90 degree
		var change = -camera_change.y
		if change + camera_angle < 70 and change + camera_angle > -90:
			$Head/Camera.rotate_x(deg2rad(change))
			camera_angle += change
		
		camera_change = Vector2()
	
#	if turnResetTimer <= 0:
#		if abs(camera_change.y) < 0.3:
#			tempPitch = 0
#		if abs(camera_change.x) < 0.3:
#			yaw = 0
#		turnResetTimer = TURN_RESET_COOLER
	
	if currentState >= STAUNDIN and _lookTurnIntensity > 0:
		look_turn()

func resetAim():
	yaw = 0
	pitch = 0
	look_turn()

func canAim():
	if has_node("Head/Camera/FPS Arms"):
		if $"Head/Camera/FPS Arms".currentState <= $"Head/Camera/FPS Arms".PAUSE:
			return false
	return true

func look_turn():
	if turnTimer <= 0:
		var tempPitch = pitch - prevPitch
		var tempYaw = yaw - prevYaw
		prevPitch = pitch
		prevYaw = yaw
		
		emit_signal("turnArms", tempPitch * _lookTurnIntensity, tempYaw * _lookTurnIntensity)
		turnTimer = TURN_COOLER

func explosion_setback(explosion):
	var max_distance = 150
	if translation.distance_squared_to(explosion.translation) < max_distance:
		var distance_percentage = (translation.distance_squared_to(explosion.translation) / max_distance * 100)
		self.explosion_timer = 100 - distance_percentage
		self.explosion_severity = explosion.explosionScale / 20

func changeArmour(amoont):
	armour += calculateArmourGain(amoont, false)
	if amoont > 0:
		$SoondFX/Armour.play(0)
	emit_signal("addArmour", armour)

func changeAmmo(wapen, amoont):
	var sound = emit_signal("playSFX", "Ammo Crate")
	if amoont <= 0:
		amoont = 1
	if sound != null:
		emit_signal("applySFXRange", global_transform.origin, 8, sound.stream.get_length(), "AmmoCrate")
	if wapen < 0:
		emit_signal("addAmmo", amoont)
	else:
		emit_signal("addWapenAmmo", wapen, amoont)

func calculateHealthLoss(amount):
	if armour > 0:
		if armour >= amount:
			armour += amount
			amount /= 2
		else:
			armour = 0
			amount /= 1.5
	return amount

func calculateHealthGain(amoont, extrae):
	if !extrae:
		if (defaultHealth - health) >= amoont:
			return amoont
		else:
			return (defaultHealth - health)

func calculateArmourGain(amoont, extrae):
	if !extrae:
		if (defaultArmour - armour) >= amoont:
			return amoont
		else:
			return (defaultArmour - armour)

func minusY(vector3):
	return Vector3(vector3.x, translation.y, vector3.z)

func turn_to(newDirection, delta):
	var new_direction = (newDirection - global_transform.origin).normalized()
	new_direction.y = translation.y
	
	look_direction = look_direction.linear_interpolate(new_direction, delta)
	
	$"Head".look_at(translation + look_direction, Vector3(0,1,0))

#This is tae set the current state, which is for rinnin, jimping, staundin, etc.
func setState(value):
	if value != currentState and currentState != DEED:
		previousState = currentState
		currentState = value
		match currentState:
			INACTIVE:
				emit_signal("setActive", false)
				set_process(false)
				set_physics_process(false)
				$Heid.enabled = false
				hide()
			LEVELEND:
				emit_signal("setActive", false)
				set_process(false)
				set_physics_process(false)
				$Heid.enabled = false
				hide()
			DEED:
				emit_signal("setActive", false)
				set_process(false)
				set_physics_process(false)
				$Heid.enabled = false
			STAUNDIN:
				set_process(true)
				set_physics_process(true)
				defaultState = currentState
				$Heid.enabled = false
				mouse_sensitivity = _mouse_sensitivity
				emit_signal("setActive", true)
				show()
				self.currentAnim = "idle"
				currentPlayerAnim = "MachinieIdle"
			JIMP:
				self.currentAnim = "idle"
				currentPlayerAnim = "Jimp"
				if defaultState == INACTIVE:
					defaultState = STAUNDIN
			MUIVIN:
				emit_signal("setActive", true)
				show()
				set_process(true)
				set_physics_process(true)
				defaultState = currentState
				$Heid.enabled = false
				mouse_sensitivity = _mouse_sensitivity
				self.currentAnim = "walkin"
				currentPlayerAnim = "MachinieRin"
			CHEAT:
				emit_signal("setActive", false)
				set_process(false)
				set_physics_process(false)
				$Heid.enabled = false
			CROOCHIN:
				set_process(true)
				set_physics_process(true)
				defaultState = currentState
				$Heid.enabled = false
				self.currentAnim = "croochin"
			CROOCHMUIVE:
				set_process(true)
				set_physics_process(true)
				$Heid.enabled = false
				previousSpeed = MAX_SPEED / 2
				self.currentAnim = "croochwalkin"
			CATCHBREATH:
				previousSpeed = MAX_REKIVERIN_SPEED
			SPRENT:
				set_process(true)
				set_physics_process(true)
				previousSpeed = MAX_RUNNING_SPEED
				$Heid.enabled = false
				self.currentAnim = "walkin"
			AIM:
				set_process(true)
				set_physics_process(true)
				defaultState = currentState
				previousSpeed = MAX_ADS_SPEED
				mouse_sensitivity = 0
				if previousState == CROOCHIN or previousState == CROOCHMUIVE:
					self.currentAnim = "croochin"
				else:
					self.currentAnim = "idle"

func getState():
	return currentState

func setAnim(anim):
	if anim != currentAnim:
		currentAnim = anim
		if has_node("animationPlayer"):
			$animationPlayer.play(currentAnim)

func naeClip():
	self.flyMode = !flyMode

func setFlyMode(value):
	flyMode = value
	if $Capsule:
		$Capsule.disabled = value
	if flyMode:
		$"Head/Camera/FPS Arms".queue_free()
		$HUD.hide()
		self.currentState = 7
		self.currentAnim = "idle"
		emit_signal("noclip")
	else:
		$"Head/Camera/FPS Arms".show()
		$HUD.show()

func activateTurbo():
	ACCEL = 50
	MAX_SPEED = 100

func activateIce():
	DEACCEL = 0.1

func activateBounce():
	bounce = !bounce

func activateMoon():
	gravity = -6

func setSprent(value):
	sprint = value
	emit_signal("chyngeSprent", sprint)

func setMouse(value):
	_mouse_sensitivity = float(value) / MOUSE_MODIFIER

func _on_joy_connection_changed(device_id, connected):
	if device_id == 0:
		controller = connected

func joy_input(aim):
	var walk = Input.get_axis("move_left_analog", "move_right_analog")
	var walkZ = Input.get_axis("move_forward_analog", "move_backward_analog")
	
	if abs(walk) > 0:
		direction += aim.x * walk
	
	if abs(walkZ) > 0:
		direction += aim.z * walkZ
	
	aim_input()
	
	if Input.is_action_just_pressed("use"):
		use()
		interact = true
	else:
		interact = false

func touch_screen_input(input):
	if input.match("use"):
		use()

func aim_input():
	if currentState > DEED and canAim():
		var lookX = Input.get_axis("look_left", "look_right")
		var lookY
		if !graphics.inverseY:
			lookY = Input.get_axis("look_up", "look_doun")
		else:
			lookY = Input.get_axis("look_doun", "look_up")

		if lookTimer <= 0:
			camera_change = Vector2(lookX * 3, lookY) * (mouse_sensitivity * 3)
			yaw = fmod(yaw - lookX, 360)
			pitch = max(min(pitch - lookY, 90), -90)
			lookTimer = LOOK_COOLER
			if lookTurnIntensity > 0:
				_lookTurnIntensity = 5
"""
