extends Node

@export var eye_moisture = 10
@onready var eye_timer = $EyeCountdown
@onready var eye_tick = $EyeTick
signal eye_dryer
signal remember_to_close_your_eyes
# Called when the node enters the scene tree for the first time.
func _ready():
	PlayerStats.eye_wetness = eye_moisture
	PlayerStats.current_eye_wetness = eye_moisture
	eye_timer.wait_time = eye_moisture
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_player_eyes_opened():
	eye_timer.start()
	eye_tick.start()

func _on_player_eyes_closed():
	PlayerStats.eye_wetness = eye_moisture
	eye_timer.stop()
	eye_tick.stop()


func _on_eye_tick_timeout():
	PlayerStats.current_eye_wetness = eye_timer.time_left
	if eye_timer.time_left < eye_moisture / 2:
		remember_to_close_your_eyes.emit()
	eye_dryer.emit()
