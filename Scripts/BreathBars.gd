extends Control

@onready var breath_in_bar = $BreatheIn
@onready var breath_out_bar = $BreatheOut
@onready var out_of_breath_bar = $OutOfBreath
var out_of_breath_max
# Called when the node enters the scene tree for the first time.
func _ready():
	var max_val = PlayerStats.released_breath
	breath_in_bar.max_value = max_val
	breath_in_bar.value = PlayerStats.held_breath
	breath_out_bar.max_value = max_val
	breath_out_bar.value = PlayerStats.released_breath
	out_of_breath_max = PlayerStats.time_to_hold_breath
	out_of_breath_bar.max_value = out_of_breath_max
	out_of_breath_bar.value = PlayerStats.time_to_hold_breath
	
	



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_player_breathing():
	breath_in_bar.value = PlayerStats.held_breath
	breath_out_bar.value = PlayerStats.released_breath
	out_of_breath_bar.value = PlayerStats.current_max_time_to_hold_breath
	


func _on_player_losing_breath():
	out_of_breath_bar.value = PlayerStats.time_to_hold_breath
	


func _on_player_breathing_stopped():
	out_of_breath_bar.max_value = PlayerStats.current_max_time_to_hold_breath
	out_of_breath_bar.value = PlayerStats.time_to_hold_breath
