extends Label


# Called when the node enters the scene tree for the first time.
func _ready():
	hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_player_remember_to_breathe(to_breathe_out):
	
	if to_breathe_out:
		text = "REMEMBER TO BREATHE! HOLD E TO BREATHE OUT (UNTIL THE BAR REACHES MAX)"
	else:
		text = "REMEMBER TO BREATHE! HOLD Q TO BREATHE IN (UNTIL THE BAR REACHES MAX)"
	show()


func _on_player_breathing():
	hide()
