extends Node

var health: float
var held_breath: float
var released_breath: float
var time_to_hold_breath: float
var current_max_time_to_hold_breath: float
var eye_wetness: float
var current_eye_wetness: float
var bullets_remaining: Array


func set_health(object, health):
	if object.is_in_group("player"):
		self.health = health
