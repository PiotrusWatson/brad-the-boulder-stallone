extends ProgressBar

# Called when the node enters the scene tree for the first time.
func _ready():
	max_value = PlayerStats.health
	value = PlayerStats.health


# Called every frame. 'delta' is the elapsed time since the previous frame.



func _on_player_ow_i_got_hurt():
	value = PlayerStats.health


func _on_player_ow_i_got_breath_pain():
	value = PlayerStats.health
