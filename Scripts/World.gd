extends Node3D

@onready var player = $Player
# Called when the node enters the scene tree for the first time.
func _ready():
	var enemies = get_tree().get_nodes_in_group("enemies")
	for enemy in enemies:
		player.connect("gun_hit_something", enemy._on_player_gun_hit_something)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	get_tree().call_group("enemies", "update_target_location", player.global_position)

func _on_enemy_bullet_spawned(bullet):
	if !bullet:
		return
	bullet.hurt_player.connect(player._on_enemy_enemy_hit_something)
