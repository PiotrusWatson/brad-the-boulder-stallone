extends ColorRect

@onready var blur_tick = $BlurTick
@export var blur_increment = 0.1
var blur_amount = 0
# Called when the node enters the scene tree for the first time.






func _on_player_eyes_dried():
	blur_tick.start()
	blur_amount += blur_increment
	material.set_shader_parameter("lod", blur_amount)


func _on_blur_tick_timeout():
	blur_amount += blur_increment
	material.set_shader_parameter("lod", blur_amount)


func _on_player_eyes_closed():
	blur_tick.stop()
	blur_amount = 0
	material.set_shader_parameter("lod", blur_amount)
	
