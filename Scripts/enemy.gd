extends CharacterBody3D

@onready var agent := $NavigationAgent3D
@onready var cover_finder = $CoverFinder
@onready var cover_timer = $TimeInCover
@export var speed = 5
@export var strafe_speed = 1
@export var bulletForce = 3
@export var damage = 30
@export var distance_to_location = 1.3
var bullet = preload("res://bullet.tscn")
var in_cover = false
var moving_out = false
signal enemy_hit_something(damage, target)
signal bullet_spawned(bullet)
var aim_target
var cover_stuff
enum EnemyState{DEFAULT, SEEN_PLAYER, AT_COVER}
var state = EnemyState.DEFAULT
var cover_target

func _ready():
	cover_stuff = get_tree().get_nodes_in_group("cover")
#move towards wherever the target location is (forever)
func _physics_process(delta):
	var current_location = global_transform.origin
	if state == EnemyState.DEFAULT:
		pass
	elif state == EnemyState.SEEN_PLAYER:
		var next_location = agent.get_next_path_position()
		var new_velocity = (next_location - current_location).normalized() * speed
		velocity = new_velocity
		if cover_target.global_position.distance_to(current_location) <= distance_to_location:
			in_cover = true
			cover_timer.start()
			state = EnemyState.AT_COVER
	else:
		velocity = Vector3.ZERO
		if moving_out:
			in_cover = false
			cover_finder.target_position = aim_target
			cover_finder.force_raycast_update()
			var hit = cover_finder.get_collider()
			if hit and !hit.is_in_group("player"):
				var player_walk_vector = (aim_target - global_position).normalized()
				var perpendicular = get_perpendicular_vector(player_walk_vector, 1) * strafe_speed
				velocity = perpendicular
		else:
			pass
		
	move_and_slide()
func get_perpendicular_vector(vector: Vector3, direction):
	var temp_vector := vector
	vector.x = vector.z
	vector.z = temp_vector.x
	
	if direction == 0:
		vector.x *= -1
	else:
		vector.z *= -1
	return vector
	
	
	
func _process(delta):
	look_at(aim_target)


func update_target_location(target_location):
	aim_target = target_location


func _on_player_gun_hit_something(damage, target):
	if target == self:
		var health = $Health
		health.get_hit(damage)


func _on_damage_zone_hit_the_thing_im_targeting(damage, thing):
	enemy_hit_something.emit(damage, thing)


func _on_run_zone_body_entered(body):
	var cover_behind = []
	if !body.is_in_group("player") or state != EnemyState.DEFAULT:
		return
	for cover in cover_stuff:
		cover_finder.target_position = cover.global_position
		cover_finder.force_raycast_update()
		var cover_checked = cover_finder.get_collider()
		if cover_checked:
			cover_behind.append(cover)
	
	if cover_behind.size() == 0:
		cover_target = global_position
	else:
		var selection = randi_range(0, cover_behind.size() - 1)
		cover_target = cover_behind[selection]
		agent.target_position = cover_target.global_position
	state = EnemyState.SEEN_PLAYER
	
	

func _on_time_between_shots_timeout():
	var real_bullet = bullet.instantiate()
	get_tree().current_scene.add_child(real_bullet)
	real_bullet.global_position = $EnemyModel/Armature/Skeleton3D/BoneAttachment3D/Handcannon/Tip.global_position
	real_bullet.damage = damage
	var direction = (aim_target - real_bullet.global_position).normalized()
	real_bullet.add_constant_force(direction * bulletForce)
	bullet_spawned.emit(real_bullet)
	
	


func _on_time_in_cover_timeout():
	moving_out = true
