extends ColorRect

@export var max_alpha = 0.3
@export var cooldown_timer = 0.5
var tween

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_player_ow_i_got_hurt():
	if tween:
		tween.kill()
	color.a = max_alpha
	tween = create_tween()
	tween.tween_property(self, "color:a", 0, cooldown_timer)
		
		
