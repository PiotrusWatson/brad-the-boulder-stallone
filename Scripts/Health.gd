extends Node

@export var maxHealth = 100
var health
signal im_ded
signal breath_pain
# Called when the node enters the scene tree for the first time.
func _ready():
	health = maxHealth
	#hacky way to reuse this code honestly i shouldn'ta even bothered trying to make the health generic
	PlayerStats.set_health($"..", health)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func get_hit(damage):
	health -= damage
	PlayerStats.set_health($"..", health)
	if health < 0:
		im_ded.emit()


func _on_hurt_tick_timeout():
	get_hit(5)
	breath_pain.emit()
