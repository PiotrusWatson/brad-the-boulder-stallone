extends Area3D

@export var group_to_target = "player"
@export var damage = 20
var target
var attacking_something = false
@onready var damage_timer = $DamageTimer

signal hit_the_thing_im_targeting(damage, thing)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_body_entered(body):
	if body.is_in_group(group_to_target) and body.is_in_group("health"):
		target = body
		hit_the_thing_im_targeting.emit(damage, body)
		damage_timer.start()
		attacking_something = true
		
		


func _on_damage_timer_timeout():
	if !attacking_something:
		damage_timer.stop()
		return
	
	hit_the_thing_im_targeting.emit(damage, target)


func _on_body_exited(body):
	attacking_something = false
	target = null
