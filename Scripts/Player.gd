extends CharacterBody3D


const SPEED = 5.0
const JUMP_VELOCITY = 4.5

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var is_dead = false

@onready var neck := $Neck
@onready var camera := $Neck/GunCamera
@onready var breath := $Breath

signal dead
signal gun_hit_something(damage, target)
signal i_performed_the_actions_requiring_me_to_win
signal ow_i_got_hurt()
signal breathing
signal losing_breath
signal ow_i_got_breath_pain
signal breathing_stopped
signal eyes_closed
signal eyes_opened
signal eyes_drying
signal eyes_dried
signal eyes_closed_too_long
signal remember_to_close_your_eyes
signal remember_to_breathe(to_breathe_out)

var can_win = false
@onready var health = $Health

#Camera moving code
func _unhandled_input(event: InputEvent) -> void :
	if is_dead:
		return
	if event is InputEventMouseButton:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	elif event.is_action_pressed("ui_cancel"):
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		if event is InputEventMouseMotion:
			neck.rotate_y(-event.relative.x * 0.01)
			camera.rotate_x(-event.relative.y * 0.01)
			camera.rotation.x = clamp(camera.rotation.x, deg_to_rad(-60), deg_to_rad(60))
			
		if event.is_action_pressed("Shoot"):
			camera.shoot()
		if event.is_action_pressed("Use_Terminal") and can_win:
			i_performed_the_actions_requiring_me_to_win.emit()
		
		if event.is_action_pressed("Breathe_In"):
			breath.start_breathing_in()
		elif event.is_action_released("Breathe_In"):
			breath.stop_breathing_in()
		elif event.is_action_pressed("Breathe_Out"):
			breath.start_breathing_out()
		elif event.is_action_released("Breathe_Out"):
			breath.stop_breathing_out()
			
		if event.is_action_pressed("Open_Eyes"):
			$ShutEyeTimer.stop()
			eyes_opened.emit()
		elif event.is_action_pressed("Close_Eyes"):
			$ShutEyeTimer.start()
			eyes_closed.emit()
			


#Player moving code
func _physics_process(delta):
	if is_dead:
		return
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("Strafe_Left", "Strafe_Right", "Forward", "Backward")
	var direction = (neck.transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)

	move_and_slide()



func _on_gun_camera_i_hit_something(damage, thing_i_hit):
	gun_hit_something.emit(damage, thing_i_hit)


func _on_win_state_press_x_to_win():
	can_win = true


func _on_win_state_cant_win_now():
	can_win = false


func _on_enemy_enemy_hit_something(damage, target):
	if target == self:
		health.get_hit(damage)
		ow_i_got_hurt.emit()


func _on_health_im_ded():
	is_dead = true
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	dead.emit()
	


func _on_breath_breathing_in():
	breathing.emit()


func _on_breath_breathing_out():
	breathing.emit()


func _on_breath_tick_timeout():
	losing_breath.emit()


func _on_health_breath_pain():
	ow_i_got_breath_pain.emit()


func _on_breath_breathing_stopped():
	breathing_stopped.emit()


func _on_eye_countdown_timeout():
	eyes_dried.emit()


func _on_eye_water_eye_dryer():
	eyes_drying.emit()


func _on_shut_eye_timer_timeout():
	eyes_closed_too_long.emit()


func _on_eye_water_remember_to_close_your_eyes():
	remember_to_close_your_eyes.emit()


func _on_breath_remember_to_breathe(to_breathe_out):
	remember_to_breathe.emit(to_breathe_out)
