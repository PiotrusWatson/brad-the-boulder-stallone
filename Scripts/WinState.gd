extends Area3D

@onready var collider = $CollisionShape3D
#could replace with an array of the scenes we wanna do if we want levels
var win_scene = "res://Scenes/Levels/win_scene.tscn"

signal press_x_to_win
signal cant_win_now
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.

func _on_body_entered(body):
	if body.is_in_group("player"):
		print("i can win")
		press_x_to_win.emit()

		
func _on_body_exited(body):
	if body.is_in_group("player"):
		print("i cant win")
		cant_win_now.emit()


func _on_player_i_performed_the_actions_requiring_me_to_win():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	get_tree().change_scene_to_file(win_scene)
	
	
