extends Node

signal breathing_in
signal breathing_out
signal breathing_stopped
signal remember_to_breathe(to_breathe_out)

var max_wait_time
var to_breathe_out = false
@export var lung_space = 40
@export var amount_to_breathe = 5
@onready var lung_timer = $TimeUntilGettingHurt
@onready var hurt_timer = $HurtTick
@onready var breathe_in_timer = $BreatheInTick
@onready var breathe_out_timer = $BreatheOutTick
@onready var lung_tick = $BreathTick

#mess of reused code lol but i chose to have different stats for these identical things :)
# Called when the node enters the scene tree for the first time.
func _ready():
	PlayerStats.held_breath = 0
	PlayerStats.released_breath = lung_space
	max_wait_time = lung_timer.wait_time
	PlayerStats.current_max_time_to_hold_breath = lung_timer.wait_time
	PlayerStats.time_to_hold_breath = lung_timer.wait_time
	lung_timer.start()
	lung_tick.start()

func calculate_new_wait_time():
	var longest_breath = PlayerStats.held_breath if PlayerStats.held_breath > PlayerStats.released_breath else PlayerStats.released_breath
	var percent = longest_breath / lung_space
	return max_wait_time * percent
	
func breathe_in():
	breathing_in.emit()
	PlayerStats.held_breath += amount_to_breathe
	PlayerStats.released_breath = 0

func breathe_out():
	breathing_out.emit()
	PlayerStats.held_breath = 0 
	PlayerStats.released_breath += amount_to_breathe

func start_breathing_out():
	if PlayerStats.released_breath >= lung_space or !to_breathe_out:
		stop_breathing_out()
		return
	hurt_timer.stop()
	lung_timer.stop()
	lung_tick.stop()
	breathe_out()
	breathe_out_timer.start()

func stop_breathing_out():
	lung_timer.wait_time = calculate_new_wait_time()
	PlayerStats.current_max_time_to_hold_breath = lung_timer.wait_time
	PlayerStats.time_to_hold_breath = lung_timer.wait_time
	if lung_timer.is_stopped():
		lung_timer.start()
		lung_tick.start()
	breathe_out_timer.stop()
	to_breathe_out = false
	breathing_stopped.emit()

func start_breathing_in():
	if PlayerStats.held_breath >= lung_space or to_breathe_out:
		stop_breathing_in()
		return
	hurt_timer.stop()
	lung_timer.stop()
	lung_tick.stop()
	breathe_in()
	breathe_in_timer.start()

func stop_breathing_in():
	lung_timer.wait_time = calculate_new_wait_time()
	PlayerStats.current_max_time_to_hold_breath = lung_timer.wait_time
	PlayerStats.time_to_hold_breath = lung_timer.wait_time
	if lung_timer.is_stopped():
		lung_timer.start()
		lung_tick.start()
	breathe_in_timer.stop()
	to_breathe_out = true
	breathing_stopped.emit()

func _on_time_until_getting_hurt_timeout():
	hurt_timer.start()


func _on_breathe_out_tick_timeout():
	breathe_out()

func _on_breathe_in_tick_timeout():
	breathe_in()


func _on_breath_tick_timeout():
	PlayerStats.time_to_hold_breath = lung_timer.time_left
	if lung_timer.time_left < PlayerStats.current_max_time_to_hold_breath / 2:
		remember_to_breathe.emit(to_breathe_out)
