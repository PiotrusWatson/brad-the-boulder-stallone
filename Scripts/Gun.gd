
extends Node3D

@onready var tip := $GunModel/Tip
@onready var shooter := $RaycastShooter
@onready var muzzle_flash = $GunModel/Tip/MuzzleFlash
@onready var cooldown_timer = $CooldownTimer
#todo: stupid reload anim spin
#todo: jamming

@export var max_ammo = 6
@export var damage = 30

signal i_hit_something(damage, thing_i_hit)


# Called when the node enters the scene tree for the first time.
func _ready():
	shooter.enabled = false
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func shoot():
	if !cooldown_timer.is_stopped():
		return
		
	cooldown_timer.start()
	shooter.enabled = true
	muzzle_flash.emitting = true
	shooter.force_raycast_update()
	var target = shooter.get_collider()
	var target_pos = shooter.get_collision_point()
	var end_line_pos = Vector3.ZERO
	
	if target != null:
		end_line_pos = target_pos
		i_hit_something.emit(damage, target)
	else:
		end_line_pos = to_global(shooter.target_position)
		
	DrawLine3d.DrawLine(tip.global_position, end_line_pos, Color.LIGHT_YELLOW, 0.3)
	shooter.enabled = false
		
	
	
	
