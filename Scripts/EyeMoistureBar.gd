extends ProgressBar


# Called when the node enters the scene tree for the first time.
func _ready():
	max_value = PlayerStats.eye_wetness
	value = PlayerStats.eye_wetness


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_player_eyes_drying():
	value = PlayerStats.current_eye_wetness



func _on_player_eyes_closed():
	value = max_value
