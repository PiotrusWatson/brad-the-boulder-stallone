extends Control


@export var speed = 0.3
@export var max_distance = 800
@onready var top_eyelid = $Top
@onready var bottom_eyelid = $Bottom

var top_tween
var bottom_tween

var original_top_position
var original_bottom_position
var new_top_position
var new_bottom_position
# Called when the node enters the scene tree for the first time.
func _ready():
	original_top_position = top_eyelid.global_position
	original_bottom_position = bottom_eyelid.global_position
	new_top_position = original_top_position
	new_top_position.y = -max_distance
	new_bottom_position = original_bottom_position
	new_bottom_position.y = max_distance
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func open_eyes():
	if top_tween:
		top_tween.kill()
	if bottom_tween:
		bottom_tween.kill()
		
	top_tween = create_tween()
	bottom_tween = create_tween()
	top_tween.tween_property(top_eyelid, "global_position", new_top_position, speed)
	bottom_tween.tween_property(bottom_eyelid, "global_position", new_bottom_position, speed)
	
func close_eyes():
	if top_tween:
		top_tween.kill()
	if bottom_tween:
		bottom_tween.kill()
	
	top_tween = create_tween()
	bottom_tween = create_tween()
	top_tween.tween_property(top_eyelid, "global_position", original_top_position, speed)
	bottom_tween.tween_property(bottom_eyelid, "global_position", original_bottom_position, speed)


func _on_player_eyes_closed():
	close_eyes()


func _on_player_eyes_opened():
	open_eyes()
