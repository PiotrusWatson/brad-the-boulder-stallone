extends Label


# Called when the node enters the scene tree for the first time.
func _ready():
	hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_win_state_press_x_to_win():
	show()


func _on_win_state_cant_win_now():
	hide()
