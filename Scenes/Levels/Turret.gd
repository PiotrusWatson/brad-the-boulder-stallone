extends CharacterBody3D


const SPEED = 5.0
const JUMP_VELOCITY = 4.5
var target

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
func update_target_location(target_location):
	target = target_location

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	look_at(target)

	move_and_slide()
