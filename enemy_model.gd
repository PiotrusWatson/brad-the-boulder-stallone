extends Node3D

@onready var skellington = $Armature/Skeleton3D
@onready var animator = $AnimationPlayer
var hand
# Called when the node enters the scene tree for the first time.
func _ready():
	animator.play("Rinnin")
	hand = skellington.get_bone_global_pose(skellington.find_bone("hand.R"))


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	get_tree().call_group("hand", "update_location", to_global(hand.origin))
	
